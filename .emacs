(setq inhibit-startup-screen t) ;; no emacs default splash screen
(setq initial-buffer-choice "~/Documents/notes/index.org") ;; define startup file
(setq org-todo-keywords
      '((sequence "TODO" "PROGRESSING" "FOLLOW UP" "DONE"))) ;; order of org-todo-toggle
(setq org-startup-folded t)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c C-l") 'org-insert-link)
(menu-bar-mode -1) ;; remove menubar
(toggle-scroll-bar -1) ;; remove scrollbar
(tool-bar-mode -1) ;; remove toolbar
(global-visual-line-mode t) ;; global visual line mode
(require 'ox-md) ;; markdown parser for org mode exports
(add-hook 'org-mode-hook 'org-indent-mode) ;; enable persistent org-indent-mode

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(tango-dark))
 '(initial-frame-alist '((fullscreen . maximized)))
 '(org-agenda-files
   '("~/Documents/notes/linuxPlusCertification.org" "~/Documents/notes/index.org"))
 '(org-directory "~/Documents/notes")
 '(package-selected-packages '(markdown-mode)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'package)
(add-to-list 'package-archives
	     '("melpa stable" . "https://stable.melpa.org/packages/"))
(package-initialize)

(add-to-list 'org-emphasis-alist
	     '("~" (:foreground "red")
	       ))
